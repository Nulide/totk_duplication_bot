#include "switch_tinyusb.h"
Adafruit_USBD_HID G_usb_hid;
NSGamepad Gamepad(&G_usb_hid);


const int btnPressTime = 35;
const int btnSlowestReleaseDelay = 600;
const int btnSlowerReleaseDelay = 350;
const int btnSlowReleaseDelay = 180;
const int btnFastReleaseDelay = 95;
const int btnPlusReleaseDelay = 120;
void setup() {
  Serial.begin(9600);
  Gamepad.begin();
  while( !USBDevice.mounted() ) delay(1);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  delay(btnSlowReleaseDelay);
  Gamepad.press(4);
  Gamepad.press(5);
  delay(btnSlowReleaseDelay);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(50);
  Gamepad.releaseAll();
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(500);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(3000);
}
void loop() {
  Gamepad.press(6);
  Gamepad.press(3);
  Gamepad.press(2);
  delay(btnSlowReleaseDelay);
  if ( Gamepad.ready() ) Gamepad.loop();
  Gamepad.release(6);
  Gamepad.release(3);
  Gamepad.release(2);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnSlowReleaseDelay);
  pressButton(9);
  delay(btnSlowestReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressDpad(4);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  Gamepad.press(0);
  Gamepad.press(1);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(35);
  Gamepad.release(0);
  Gamepad.release(1);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(1500);
  moveAxis(0);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  moveAxis(1);
  delay(btnSlowReleaseDelay);
}

void pressButton(int id){
  Gamepad.press(id);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnPressTime);
  Gamepad.release(id);
  if ( Gamepad.ready() ) Gamepad.loop();
}

void pressDpad(int id){
  Gamepad.dPad(id);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnPressTime);
  Gamepad.dPad(false, false, false, false);
  if ( Gamepad.ready() ) Gamepad.loop();
}

void moveAxis(int id){
  if (id == 0){
    Gamepad.leftYAxis(0xFF);
  }else if(id == 1){
    Gamepad.leftYAxis(0x00);
  }
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnPressTime);
    Gamepad.leftYAxis(0X80);
  if ( Gamepad.ready() ) Gamepad.loop();
}
