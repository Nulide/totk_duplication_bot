# TOTK_DUPLICATION_BOT

This Project aims to automatize the Duplication Glitch found in The Legend of Zelda Tears of the Kindgom.

## Getting started

1. Install the Arduino IDE

2. Install the following libraries:
    - Adafruit TinyUSB Library
    - Pico PIO USB

3. You need to install the RP2040 board package from Earle F. Philhower, III in the Board Manager

4. Select your board

5. Select Tools -> USB Stack -> Adafruit TinyUSB

6. Compile and run.

7. Now when you start the game you need to have at least 7 spaces for bows. Than select the one before the last and move the cursor to the last bow.

8. Insert the chip and that's it. :)
