#include "switch_tinyusb.h"
Adafruit_USBD_HID G_usb_hid;
NSGamepad Gamepad(&G_usb_hid);


const int btnPressTime = 35;
const int btnSlowestReleaseDelay = 600;
const int btnSlowerReleaseDelay = 350;
const int btnSlowReleaseDelay = 180;
const int btnFastReleaseDelay = 95;
const int btnPlusReleaseDelay = 120;
void setup() {
  Serial.begin(9600);
  Gamepad.begin();
  while( !USBDevice.mounted() ) delay(1);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  delay(btnSlowReleaseDelay);
  Gamepad.press(4);
  Gamepad.press(5);
  delay(btnSlowReleaseDelay);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(50);
  Gamepad.releaseAll();
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(500);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(1000);
}
void loop() {
  pressButton(7);
  delay(btnSlowReleaseDelay);
  pressDpad(NSGAMEPAD_DPAD_UP);
  delay(btnSlowReleaseDelay);
  pressButton(2);
  delay(btnSlowestReleaseDelay);
  pressButton(9);
  delay(btnSlowestReleaseDelay);
  pressDpad(NSGAMEPAD_DPAD_LEFT);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressDpad(NSGAMEPAD_DPAD_DOWN);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressDpad(NSGAMEPAD_DPAD_RIGHT);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnSlowestReleaseDelay);
  pressButton(9);
  delay(btnPlusReleaseDelay);
  pressButton(9);
  delay(btnSlowestReleaseDelay);
  pressDpad(NSGAMEPAD_DPAD_LEFT);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressDpad(NSGAMEPAD_DPAD_DOWN);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  pressButton(9);
  delay(btnSlowReleaseDelay);
  moveAxis(0);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnFastReleaseDelay);
  pressButton(2);
  delay(btnSlowReleaseDelay);
  moveAxis(1);
  delay(btnFastReleaseDelay);
}

void pressButton(int id){
  Gamepad.press(id);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnPressTime);
  Gamepad.release(id);
  if ( Gamepad.ready() ) Gamepad.loop();
}

void pressDpad(int id){
  Gamepad.dPad(id);
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnPressTime);
  Gamepad.dPad(false, false, false, false);
  if ( Gamepad.ready() ) Gamepad.loop();
}

void moveAxis(int id){
  if (id == 0){
    Gamepad.leftYAxis(0xFF);
  }else if(id == 1){
    Gamepad.leftYAxis(0x00);
  }
  if ( Gamepad.ready() ) Gamepad.loop();
  delay(btnPressTime);
    Gamepad.leftYAxis(0X80);
  if ( Gamepad.ready() ) Gamepad.loop();
}
